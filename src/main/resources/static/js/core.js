/**
 * Created by mzlion on 2017/3/22.
 */
$(function () {
    hljs.initHighlightingOnLoad();
    $('.highlight').each(function () {
        $(this).before('<div class="zero-clipboard"><span class="btn-clipboard">复制</span></div>');
    });
    ZeroClipboard.config({
        hoverClass: 'btn-clipboard-hover'
    });
    var client = new ZeroClipboard(document.querySelectorAll('.btn-clipboard')),
        $htmlBrige = $("#global-zeroclipboard-html-bridge");
        $htmlBrige.data('placement', 'top').attr('title', '复制到粘贴板').tooltip();
    client.on('ready', function () {
        client.on('copy', function (event) {
            event.clipboardData.setData('text/plain', $(event.target).parent().next('.highlight').text());
        });
    });
});