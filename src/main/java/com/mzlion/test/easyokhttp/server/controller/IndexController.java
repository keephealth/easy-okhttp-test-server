package com.mzlion.test.easyokhttp.server.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * 首页
 *
 * @author mzlion on 2016/12/10.
 */
@Controller
public class IndexController {

    @Value("${custom.basePath}")
    private String basePath;

    /**
     * 首页测试地址
     *
     * @return 返回字符串内容
     */
    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String index(Map<String,Object> model) {
        model.put("basePath", basePath);
        return "index";
    }
}
