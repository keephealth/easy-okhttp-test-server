package com.mzlion.test.easyokhttp.server.dto;

import java.io.Serializable;

/**
 * Created by mzlion on 2017/3/23.
 */
public class FileData implements Serializable {

    private static final long serialVersionUID = 9026768987045054981L;

    private String oldFilename;
    private String newFilename;

    private int fileSize;
    private String fileExt;

    private String showUrl;

    public String getOldFilename() {
        return oldFilename;
    }

    public void setOldFilename(String oldFilename) {
        this.oldFilename = oldFilename;
    }

    public String getNewFilename() {
        return newFilename;
    }

    public void setNewFilename(String newFilename) {
        this.newFilename = newFilename;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileExt() {
        return fileExt;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    public String getShowUrl() {
        return showUrl;
    }

    public void setShowUrl(String showUrl) {
        this.showUrl = showUrl;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FileData{");
        sb.append("oldFilename='").append(oldFilename).append('\'');
        sb.append(", newFilename='").append(newFilename).append('\'');
        sb.append(", fileSize=").append(fileSize);
        sb.append(", fileExt='").append(fileExt).append('\'');
        sb.append(", showUrl='").append(showUrl).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
