package com.mzlion.test.easyokhttp.server.dto;

import java.io.Serializable;

/**
 * Created by mzlion on 2017/3/23.
 */
public class TaoBaoIpInfo implements Serializable {

    private static final long serialVersionUID = 1540937788463302421L;
    /**
     * 接口状态码
     */
    private int code;

    private Object data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TaoBaoIpInfo{");
        sb.append("code=").append(code);
        sb.append(", data='").append(data).append('\'');
        sb.append('}');
        return sb.toString();
    }

}

