package com.mzlion.test.easyokhttp.server.controller;

import com.mzlion.core.json.TypeRef;
import com.mzlion.easyokhttp.HttpClient;
import com.mzlion.test.easyokhttp.server.dto.ApiResult;

import java.util.Map;

/**
 * Created by mzlion on 2017/3/27.
 */
public class Temp {

    public void test() throws Exception {
        ApiResult<Map<String, Object>> ipInfo = HttpClient
                .get("https://project.mzlion.com/easy-okhttp/api/ip-info")
                .queryString("ip", "112.64.217.29")
                .asBean(new TypeRef<ApiResult<Map<String, Object>>>() {
                });
    }
}
